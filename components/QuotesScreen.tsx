import { StatusBar } from "expo-status-bar";
import { useEffect, useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";

const getQuote = async () => {
	const response = await fetch("https://api.quotable.io/random");
	const data = await response.json();
	return data;
};

interface Quote {
	_id: string;
	content: string;
}

export function QuotesScreen() {
	const [quote, setQuote] = useState<Quote | null>(null);
	const fetchQuote = async () => {
		const APIquote = await getQuote();
		console.log(APIquote);
		setQuote(APIquote);
	};
	useEffect(() => {
		if (quote) return;
		fetchQuote();
	}, []);
	return (
		<View>
			<Text style={styles.textCenter}>Here is random quotes : </Text>
			{quote && <Text style={styles.quote}>{quote.content}</Text>}
			<Button title="Get new quote" onPress={fetchQuote} />
			<StatusBar style="auto" animated={true} />
		</View>
	);
}

const styles = StyleSheet.create({
	textCenter: {
		textAlign: "center",
	},
	quote: {
		fontSize: 30,
		fontStyle: "italic",
		textAlign: "center",
		paddingVertical: 20,
	},
});
