# Easeal 🍽️ Mobile Application

Easeal is a webapp where you can create some **meals** with **ingredients** and use it through your week. Every informations are simplified each ingredient has a main type (carbohydrates, sugar, fat...) and kcal/100gr.  
The application should help you to have better meals, and grade your days.  
At the end you can use a day or a week to generate your grocery list.

---

## Technical stack :

-   Mobile app run with react-native
